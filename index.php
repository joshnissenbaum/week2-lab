<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,minimum-scale=1.0, maximum-scale=1.0" />
	<title>GriffithConnection</title>
    <link href="css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

</head>
<body>
<div class="container">
    <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">GriffithConnection</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Notifications <span class="badge">0</span><span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Act</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
          <li><a href="#">Messages <span class="badge">0</span></a></li>
        </li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
             <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
      </form>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<div class="row">
  <div class="col-md-6 col-md-4">
      <h4>Welcome, Joshua Nissenbaum</h4>
      <div class="user-info">
          <ul>
              <li>You last logged in on the 1st of March</li>
              <li>There are <?php echo rand(1,10); ?> news updates</li>
          </ul>
      </div>
      
      <ul class="nav nav-pills nav-stacked">
      <li role="presentation"><a href="#">Photos</a></li>
      <li role="presentation"><a href="#">Friends</a></li>
      <li role="presentation"><a href="#">News & Articles</a></li>
    </ul>
      <hr>
      <div class="user-message">
          <h4>Update Status</h4>
          <input class="form-control" type="text" placeholder="What are you up to today?">
      </div>
        <hr>
      <div class="user-message">
          Name:<p></p>
          <input class="form-control" type="text" placeholder="First and Last name"><p></p>
          Message:<p></p>
          <input class="form-control" type="text" placeholder="Message">
      </div>
      
  </div>
  <div class="col-md-6">
    
    <?php
     $posts = array(
     array("date" => "1 March 2016", "user" => "Bob Lewis", "message"=> "Just uploaded some photos from my holiday to the server room", "image" => "1.jpg"),
     array("date" => "4 March 2016", "user" => "Mike Larry", "message"=> "Good to be here", "image" => "2.png"),
     array("date" => "16 March 2016", "user" => "Johnny Cash", "message"=> "Just uploaded some photos from my holiday to the server room", "image" => "3.jpg"),
     array("date" => "13 March 2016", "user" => "Leonardo Lupus", "message"=> "Pics from my holiday up", "image" => "4.jpg"),
     array("date" => "14 March 2016", "user" => "Steven Stelpe", "message"=> "Just saw Johnny Cash at Folsom prison", "image" => "5.jpg"),
     array("date" => "19 March 2016", "user" => "Bernardo Ramirez", "message"=> "Dammit! Left my keys at work!", "image" => "6.jpg"),
     array("date" => "13 March 2016", "user" => "Brittany Johnson", "message"=> "Car wouldn't start this morning LOL", "image" => "7.jpg"),
     array("date" => "13 March 2016", "user" => "Michael Podmore", "message"=> "Playing Rocket League. Best. Game. Ever!", "image" => "8.jpg"),
     array("date" => "18 March 2016", "user" => "Tom Cash", "message"=> "Stuck at uni all day today! Noooo!", "image" => "9.jpg"),
      array("date" => "12 March 2016", "user" => "Tayla Bertwin", "message"=> "Stuck at work all day today! Noooo!", "image" => "10.jpg"),
     );
    ?> 
    
    <?php
    $randno = rand(1, 10);
    for($i = 0; $i < $randno; $i++)
    {
      echo'
          <div class="media">
  <div class="media-left">
    <a href="#">
      <div class="small-image" style="background-image: url(images/'.$posts[$i]['image'].')" alt="...">
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">'.$posts[$i]['user'].'</h4>
    '.$posts[$i]['message'].'
    <div class="media-date">Posted on: '.$posts[$i]['date'].'</div>
  </div>
</div>
  </div><hr>';
    }
  
    
    ?>
    
</div>
    
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
</body>
</html>
