<!DOCTYPE html>
<html lang="en">
<head>
	@include('header')
</head>
<body>
<div class="container">
	@include('nav')
<div class="row">
  <div class="col-md-6 col-md-4">
     @include('left-col')
  </div>
  <div class="col-md-6">
  	@if($header == 'feed')
  		@include('main.feed')
  	@elseif($header == 'friends')
  		@include('main.friends')
  	@endif
  
</div>
    
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
</body>
</html>
