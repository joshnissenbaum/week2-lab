<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as' => 'feed', function()
{
	return View::make('layouts.default')->with('header', 'feed');
}));

Route::get('/friends', array('as' => 'friends', function()
{
	return View::make('layouts.default')->with('header', 'friends');
}));
